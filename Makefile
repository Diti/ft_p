# **************************************************************************** #
#                                                                              #
#                                                         :::      ::::::::    #
#    Makefile                                           :+:      :+:    :+:    #
#                                                     +:+ +:+         +:+      #
#    By: dtortera <dtortera@student.42.fr>          +#+  +:+       +#+         #
#                                                 +#+#+#+#+#+   +#+            #
#    Created: 2014/05/13 13:53:53 by dtortera          #+#    #+#              #
#    Updated: 2015/11/07 13:17:44 by dtortera         ###   ########.fr        #
#                                                                              #
# **************************************************************************** #

# Files
CLIENT_SRCS = $(addprefix srcs/client/, main.c socket.c loop.c)
SERVER_SRCS = $(addprefix srcs/serveur/, main.c socket.c loop.c)
COMMON_SRCS = $(addprefix srcs/, commands.c error.c \
				iosock_data.c iosock_files.c \
				cmd_cat.c cmd_cd.c cmd_get.c cmd_ls.c cmd_put.c cmd_pwd.c)

CLIENT_OBJS = $(patsubst %.c, %.o, $(CLIENT_SRCS))
SERVER_OBJS = $(patsubst %.c, %.o, $(SERVER_SRCS))
COMMON_OBJS = $(patsubst %.c, %.o, $(COMMON_SRCS))

# Compiler & linker flags
CFLAGS = -Wall -Wextra -Werror
CPPFLAGS = -iquote inc -iquote libft/inc
LDFLAGS = -L libft -lft

# Rules (42)
all: client serveur

srcs/commands.o: srcs/commands.c inc/commands.h

%.o: %.c inc/ftp.h libft/libft.a
	$(CC) $(CFLAGS) $(CPPFLAGS) -o $@ -c $<

client: $(CLIENT_OBJS) $(COMMON_OBJS)
	$(CC) $(CFLAGS) $(CPPFLAGS) $(LDFLAGS) $^ -o $@ $(LDFLAGS)

serveur: $(SERVER_OBJS) $(COMMON_OBJS)
	$(CC) $(CFLAGS) $(CPPFLAGS) $(LDFLAGS) $^ -o $@ $(LDFLAGS)

clean:
	@$(MAKE) $@ -C libft
	$(RM) $(CLIENT_OBJS)
	$(RM) $(SERVER_OBJS)
	$(RM) $(COMMON_OBJS)

fclean: clean
	@printf "RM\t%s\n" "client serveur"
	@$(RM) client serveur
	@printf "RM\t%s\n" "libft/libft.a"
	@$(RM) libft/libft.a

re: fclean all

libft/libft.a:
	@$(MAKE) -C libft

.PHONY: clean fclean re
