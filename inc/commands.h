/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   commands.h                                         :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: dtortera <dtortera@student.42.fr>          +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2014/05/17 17:58:14 by dtortera          #+#    #+#             */
/*   Updated: 2015/11/07 13:18:48 by dtortera         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#ifndef COMMANDS_H
# define COMMANDS_H

# include <stddef.h>
# include "ftp.h"

int	do_nothing(t_server *serv, int c_sock, const char *cmd_line);
int cmd_cat(t_server *serv, int c_sock, const char *cmd_line);
int	cmd_cd(t_server *serv, int c_sock, const char *cmd_line);
int	cmd_get(t_server *serv, int c_sock, const char *cmd_line);
int	cmd_put(t_server *serv, int c_sock, const char *cmd_line);
int	cmd_pwd(t_server *serv, int c_sock, const char *cmd_line);
int cmd_help(t_server *serv, int c_sock, const char *cmd_line);
int cmd_ls(t_server *serv, int c_sock, const char *cmd_line);

const struct s_cmd		g_commands[] =
{
	{ "cat", &cmd_cat },
	{ "cd", &cmd_cd },
	{ "get", &cmd_get },
	{ "help", &cmd_help },
	{ "ls", &cmd_ls },
	{ "put", &cmd_put },
	{ "pwd", &cmd_pwd },
	{ NULL, &do_nothing }
};

#endif
