/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   ftp.h                                              :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: dtortera <dtortera@student.42.fr>          +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2014/05/16 18:50:47 by dtortera          #+#    #+#             */
/*   Updated: 2015/11/07 13:09:31 by dtortera         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#ifndef FTP_H
# define FTP_H

# define CLIENT_PROMPT		"client_ftp> "
# define MAX_CONN			(42)
# define SOCK_OPTS			(SO_REUSEADDR)

# define MSG_OK				"\033[32mSUCCESS\033[0m"
# define MSG_FAIL			"\033[31mERROR\033[0m"

# define CHUNK_SIZE	1024

# include <errno.h>
# include <netdb.h>
# include <stddef.h>

enum	e_error
{
	ERR = -1,
	NO_ERR = 0,
	SUCCESS,
	FAILURE,
	ERR_CHDIR,
	ERR_GETCWD,
	ERR_GETADDRINFO,
	ERR_SOCKET,
	ERR_SETSOCKOPT,
	ERR_BIND,
	ERR_LISTEN,
	ERR_ACCEPT,
	ERR_FORK,
	ERR_CONNECT,
	ERR_SEND,
	ERR_RECV,
	ERR_CMD_NOTFOUND,
	ERR_LS_EXECV,
	ERR_OPEN,
	ERR_CLOSE,
	QUIT
};

typedef struct s_header	t_header;
typedef struct s_client	t_client;
typedef struct s_server	t_server;
typedef struct s_cmd	t_cmd;

struct	s_header
{
	ssize_t			nbr_chunk;
	ssize_t			size;
};

struct	s_client
{
	int				sock;
	struct addrinfo *addr;
	const char		*port;
	const char		*host;
};

struct	s_server
{
	char			*droot;
	int				sock;
	struct addrinfo	*addr;
	const char		*port;
};

struct	s_cmd
{
	const char	*name;
	int			(*func)(t_server *, int, const char *);
};

# define ERRNO_EXIT(p, n)	error_exit(p, ft_strerror(errno), n)

int		error_fd(const char *msg_desc, int fd);
void	error_exit(const char *prefix, const char *err_msg, int err_num);

int		recv_file(int sock, const char *filename);
int		send_file(int sock, const char *filename);
ssize_t	recv_msg(int fd, char **data);
ssize_t	send_data(int fd, const char *data, int count);
ssize_t	send_msg(int fd, const char *data);
void	send_end(int fd);

int		ftp_client_run(t_client *cl, const char *host, const char *port);
int		ftp_client_loop(t_client *cl);
int		ftp_serv_run(t_server *serv, const char *port);
int		ftp_serv_loop(t_server *serv);

#endif
