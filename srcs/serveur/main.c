/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   main.c                                             :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: dtortera <dtortera@student.42.fr>          +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2014/05/13 14:03:20 by dtortera          #+#    #+#             */
/*   Updated: 2015/11/07 12:31:43 by dtortera         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include <errno.h>
#include <signal.h>
#include <unistd.h>
#include "libft.h"
#include "ftp.h"

static int	usage(const char *prog_name)
{
	ft_putstr_fd("Usage: ", STDERR_FILENO);
	if (prog_name)
		ft_putstr_fd(prog_name, STDERR_FILENO);
	else
		ft_putstr_fd("serveur", STDERR_FILENO);
	ft_putstr_fd(" <port>", STDERR_FILENO);
	ft_putstr_fd(" [--root <dir>]\n", STDERR_FILENO);
	return (NO_ERR);
}

static void	init_serv(t_server *serv, const char *root_arg)
{
	char	*cur_dir;

	if (NULL == serv)
		error_exit("init_serv()", "t_server error", ERR);
	if (NULL == (cur_dir = getcwd(NULL, 0)))
		ERRNO_EXIT("init_serv(): getcwd()", ERR_GETCWD);
	if (NULL == root_arg)
	{
		ft_putstr_fd("Using default (server’s) root directory: ", 2);
		ft_putendl_fd(cur_dir, STDERR_FILENO);
		serv->droot = ft_strdup(cur_dir);
	}
	else
	{
		ft_putstr_fd("Using custom root directory: ", STDERR_FILENO);
		ft_putendl_fd(root_arg, STDERR_FILENO);
		serv->droot = ft_strdup(root_arg);
	}
	serv->sock = -1;
	serv->port = NULL;
}

int			main(int argc, char *argv[])
{
	enum e_error	ret;
	t_server		serv;

	if (argc < 2)
		return (usage(argv[0]));
	signal(SIGCHLD, SIG_IGN);
	if (argv[2] && ft_strequ(argv[2], "--root"))
	{
		if (argv[3])
			init_serv(&serv, argv[3]);
		else
		{
			ft_putendl_fd("Missing argument for option --root.", 2);
			init_serv(&serv, NULL);
		}
	}
	else
		init_serv(&serv, NULL);
	if ((ret = ftp_serv_run(&serv, argv[1])) != NO_ERR)
		return (ret);
	if (-1 == close(serv.sock))
		ERRNO_EXIT("[main.c] close()", ERR_CLOSE);
	return (NO_ERR);
}
