/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   socket.c                                           :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: dtortera <dtortera@student.42.fr>          +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2014/05/14 13:41:16 by dtortera          #+#    #+#             */
/*   Updated: 2015/11/07 12:51:39 by dtortera         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include <netdb.h>
#include <sys/socket.h>
#include <sys/types.h>
#include "libft.h"
#include "ftp.h"

static int	ftp_serv_socket(t_server *serv)
{
	struct addrinfo	hints;
	int				ret;

	ft_memset(&hints, 0, sizeof(hints));
	hints.ai_family = PF_INET;
	hints.ai_socktype = SOCK_STREAM;
	hints.ai_flags = AI_PASSIVE;
	ret = getaddrinfo(NULL, serv->port, &hints, &serv->addr);
	if (ret == EAI_SYSTEM)
		ERRNO_EXIT("getaddrinfo()", ERR_GETADDRINFO);
	if (ret != 0)
		error_exit("getaddrinfo()",
			"Socket setup failed. Port number invalid?", ERR_GETADDRINFO);
	ret = socket(hints.ai_family, hints.ai_socktype, hints.ai_protocol);
	if (ret == -1)
		ERRNO_EXIT("socket()", ERR_SOCKET);
	serv->sock = ret;
	return (NO_ERR);
}

int			ftp_serv_run(t_server *serv, const char *port)
{
	int	on;

	on = 1;
	serv->port = port;
	ftp_serv_socket(serv);
	if (setsockopt(serv->sock, SOL_SOCKET, SOCK_OPTS, &on, sizeof(on)) == -1)
		ERRNO_EXIT("setsockopt()", ERR_SETSOCKOPT);
	if (bind(serv->sock, serv->addr->ai_addr, serv->addr->ai_addrlen) == -1)
		ERRNO_EXIT("bind()", ERR_BIND);
	if (listen(serv->sock, MAX_CONN) == -1)
		ERRNO_EXIT("listen()", ERR_LISTEN);
	if (-1 == chdir(serv->droot))
		ERRNO_EXIT("chdir()", ERR_CHDIR);
	ft_putendl("Server ready to accept connections.");
	ftp_serv_loop(serv);
	return (NO_ERR);
}
