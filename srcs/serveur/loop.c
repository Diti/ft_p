/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   loop.c                                             :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: dtortera <dtortera@student.42.fr>          +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2014/05/16 15:23:01 by dtortera          #+#    #+#             */
/*   Updated: 2015/11/04 12:08:04 by dtortera         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include <arpa/inet.h>
#include <netinet/in.h>
#include <stdio.h>
#include <stdlib.h>
#include <sys/socket.h>
#include "libft.h"
#include "ftp.h"

static int	handle_cmd(t_server *serv, int c_sock, const char *line)
{
	extern const struct s_cmd	g_commands[];
	const char					*cmd_name;
	size_t						i;

	i = -1;
	while ((cmd_name = g_commands[++i].name) != NULL)
	{
		if (ft_strnstr(line, cmd_name, ft_strlen(cmd_name)) != NULL)
		{
			g_commands[i].func(serv, c_sock, line);
			return (NO_ERR);
		}
	}
	return (ERR_CMD_NOTFOUND);
}

static void	handle_client(const char *c_host, t_server *serv, int c_sock)
{
	char	*line;

	printf("Client \033[94m%s\033[39m connected.\n", c_host);
	while (recv_msg(c_sock, &line) > 0)
	{
		if (*line != '\0')
		{
			printf("[%s] Client sent: \033[94m%s\033[39m\n", c_host, line);
			if (handle_cmd(serv, c_sock, line) != NO_ERR)
				break ;
		}
		send_end(c_sock);
		ft_strdel(&line);
	}
	printf("Client \033[94m%s\033[39m disconnected.\n", c_host);
	exit(NO_ERR);
}

int			ftp_serv_loop(t_server *serv)
{
	int					c_sock;
	struct sockaddr_in	c_addr;
	socklen_t			c_alen;
	pid_t				pid;

	c_alen = sizeof(struct sockaddr_in *);
	while (1)
	{
		c_sock = accept(serv->sock, (struct sockaddr *)&c_addr, &c_alen);
		if (c_sock == -1)
			ERRNO_EXIT("accept()", ERR_ACCEPT);
		if ((pid = fork()) == -1)
			ERRNO_EXIT("fork()", ERR_FORK);
		if (pid == 0)
		{
			close(serv->sock);
			handle_client(inet_ntoa(c_addr.sin_addr), serv, c_sock);
		}
		else
			close(c_sock);
	}
	return (NO_ERR);
}
