/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   cmd_ls.c                                           :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: dtortera <marvin@42.fr>                    +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2015/10/13 14:14:00 by dtortera          #+#    #+#             */
/*   Updated: 2015/11/05 17:12:41 by dtortera         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "ftp.h"
#include "libft.h"
#include <errno.h>
#include <dirent.h>

int	cmd_ls(t_server *serv, int c_sock, const char *cmd_line)
{
	DIR				*dir;
	struct dirent	*ent;

	(void)cmd_line;
	(void)serv;
	if (NULL == (dir = opendir(".")))
		return (error_fd(ft_strerror(errno), c_sock));
	errno = 0;
	while ((ent = readdir(dir)) != NULL)
		send_msg(c_sock, ent->d_name);
	if (errno != 0)
		return (error_fd(ft_strerror(errno), c_sock));
	errno = 0;
	if (-1 == closedir(dir))
		return (error_fd(ft_strerror(errno), c_sock));
	send_msg(c_sock, MSG_OK);
	return (NO_ERR);
}
