/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   cmd_cd.c                                           :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: dtortera <marvin@42.fr>                    +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2015/10/28 14:19:33 by dtortera          #+#    #+#             */
/*   Updated: 2015/11/07 12:01:01 by dtortera         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "ftp.h"
#include "libft.h"
#include <errno.h>

static int	change_dir(char *droot, char *dest, char *src, int c_sock)
{
	long	i;

	if (dest == NULL)
	{
		if (-1 == chdir(droot))
			return (error_fd(ft_strerror(errno), c_sock));
	}
	else
	{
		if (-1 == chdir(dest))
			return (error_fd(ft_strerror(errno), c_sock));
		i = ft_strlen(getcwd(NULL, 0)) - ft_strlen(droot);
		if (i < 0)
		{
			if (-1 == chdir(src))
				return (error_fd(ft_strerror(errno), c_sock));
			return (error_fd("Access forbidden above document root.", c_sock));
		}
		send_msg(c_sock, MSG_OK);
	}
	return (NO_ERR);
}

int			cmd_cd(t_server *serv, int c_sock, const char *cmd_line)
{
	char	**cmd;
	char	*pwd;
	long	i;
	int		ret;

	if (NULL == (cmd = ft_strsplit(cmd_line, ' ')))
		return (ERR);
	if (NULL == (pwd = getcwd(NULL, 0)))
		return (error_fd(ft_strerror(errno), c_sock));
	i = ft_strlen(pwd) - ft_strlen(serv->droot);
	if (i < 0)
		return (error_fd("Access forbidden above document root.", c_sock));
	ret = change_dir(serv->droot, cmd[1], pwd, c_sock);
	return (ret);
}
