/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   cmd_get.c                                          :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: dtortera <marvin@42.fr>                    +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2015/11/06 16:59:57 by dtortera          #+#    #+#             */
/*   Updated: 2015/11/07 13:17:27 by dtortera         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "ftp.h"
#include "libft.h"

int	cmd_get(t_server *serv, int c_sock, const char *cmd_line)
{
	char		**files;

	(void)serv;
	files = ft_strsplit(cmd_line, ' ');
	send_file(c_sock, files[1]);
	send_msg(c_sock, MSG_OK);
	return (NO_ERR);
}
