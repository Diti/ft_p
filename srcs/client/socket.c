/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   socket.c                                           :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: dtortera <dtortera@student.42.fr>          +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2014/05/14 13:41:16 by dtortera          #+#    #+#             */
/*   Updated: 2015/10/13 13:23:42 by dtortera         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include <netdb.h>
#include <stdio.h>
#include <sys/socket.h>
#include <sys/types.h>
#include "libft.h"
#include "ftp.h"

static int	ftp_client_socket(t_client *client)
{
	struct addrinfo	hints;
	int				ret;

	ft_memset(&hints, 0, sizeof(hints));
	hints.ai_family = PF_INET;
	hints.ai_socktype = SOCK_STREAM;
	ret = getaddrinfo(client->host, client->port, &hints, &client->addr);
	if (ret == EAI_SYSTEM)
		ERRNO_EXIT("getaddrinfo()", ERR_GETADDRINFO);
	if (ret != 0)
		error_exit("getaddrinfo()",
			"Server unreachable. Socket setup failed.", ERR_GETADDRINFO);
	ret = socket(hints.ai_family, hints.ai_socktype, hints.ai_protocol);
	if (ret == -1)
		ERRNO_EXIT("socket()", ERR_SOCKET);
	client->sock = ret;
	return (NO_ERR);
}

int			ftp_client_run(t_client *cl, const char *host, const char *port)
{
	cl->host = host;
	cl->port = port;
	ftp_client_socket(cl);
	if (connect(cl->sock, cl->addr->ai_addr, cl->addr->ai_addrlen) == -1)
		ERRNO_EXIT("connect()", ERR_CONNECT);
	printf("Type \033[94m%s\033[39m for a list of available commands.\n",
		"help");
	ftp_client_loop(cl);
	return (NO_ERR);
}
