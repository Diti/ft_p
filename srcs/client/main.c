/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   main.c                                             :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: dtortera <dtortera@student.42.fr>          +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2014/05/13 14:03:20 by dtortera          #+#    #+#             */
/*   Updated: 2014/05/17 12:05:12 by dtortera         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "libft.h"
#include "ftp.h"

static void	usage(const char *prog_name)
{
	ft_putstr_fd("Usage: ", 2);
	if (prog_name)
		ft_putstr_fd(prog_name, 2);
	else
		ft_putstr_fd("client", 2);
	ft_putstr_fd(" <addr> <port>\n", 2);
}

int			main(int argc, char *argv[])
{
	enum e_error	ret;
	t_client		client;

	if (argc < 3)
	{
		usage(argv[0]);
		return (NO_ERR);
	}
	if ((ret = ftp_client_run(&client, argv[1], argv[2])) != NO_ERR)
		return (ret);
	return (NO_ERR);
}
