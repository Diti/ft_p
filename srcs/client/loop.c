/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   loop.c                                             :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: dtortera <dtortera@student.42.fr>          +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2014/05/16 15:23:01 by dtortera          #+#    #+#             */
/*   Updated: 2015/11/07 13:26:33 by dtortera         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include <arpa/inet.h>
#include <errno.h>
#include <netinet/in.h>
#include <stddef.h>
#include <stdio.h>
#include <sys/socket.h>
#include "libft.h"
#include "ftp.h"

static int	process_reply(int s_sock)
{
	char	*buf;

	while (recv_msg(s_sock, &buf) > 0)
	{
		if (buf == NULL)
			return (ERR);
		if (*buf != '\0')
		{
			printf("%s\n", buf);
			continue ;
		}
	}
	if (ft_strnstr(buf, MSG_FAIL, ft_strlen(MSG_FAIL)))
		return (FAILURE);
	else if (ft_strequ(buf, MSG_OK))
		return (SUCCESS);
	else
		return (NO_ERR);
}

static int	handle_cmd(int s_sock, char *line)
{
	const char					*cmd_name;
	char						**files;
	extern const struct s_cmd	g_commands[];
	size_t						i;

	i = -1;
	line = ft_strtrim(line);
	while ((cmd_name = g_commands[++i].name) != NULL)
	{
		if (ft_strequ("quit", line))
			return (QUIT);
		else if (ft_strnstr(line, cmd_name, ft_strlen(cmd_name)) != NULL)
		{
			send_msg(s_sock, line);
			files = ft_strsplit(line, ' ');
			if (ft_strnequ(cmd_name, "put", 3))
				send_file(s_sock, files[1]);
			else if (ft_strnequ(cmd_name, "get", 3))
				recv_file(s_sock, files[1]);
			return (process_reply(s_sock));
		}
	}
	printf("Unknown command: %s\n", line);
	return (ERR_CMD_NOTFOUND);
}

static int	prompt(char **line)
{
	ft_putstr(CLIENT_PROMPT);
	return (get_next_line(0, line));
}

int			ftp_client_loop(t_client *cl)
{
	char	*line;
	int		s_sock;
	int		ret;

	line = NULL;
	s_sock = cl->sock;
	while (prompt(&line) != 0)
	{
		ret = handle_cmd(s_sock, ft_strtrim(line));
		if (ret == QUIT)
			break ;
		else if (ret == SUCCESS || ret == FAILURE)
		{
			ft_strdel(&line);
			continue ;
		}
	}
	ft_strdel(&line);
	if (-1 == close(s_sock))
		ft_putendl_fd(ft_strerror(errno), 2);
	ft_putendl("Connection closed.");
	return (NO_ERR);
}
