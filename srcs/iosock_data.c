/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   iosock_data.c                                      :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: dtortera <dtortera@student.42.fr>          +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2014/05/17 10:50:50 by dtortera          #+#    #+#             */
/*   Updated: 2015/11/07 12:05:54 by dtortera         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "ftp.h"
#include "libft.h"
#include <errno.h>
#include <fcntl.h>
#include <stddef.h>
#include <stdio.h>
#include <sys/socket.h>

ssize_t	send_msg(int fd, const char *data)
{
	return (send_data(fd, data, ft_strlen(data) + 1));
}

ssize_t	send_data(int fd, const char *data, int count)
{
	t_header	header;
	t_header	sub_header;
	int			ret;

	header.size = count;
	header.nbr_chunk = count / CHUNK_SIZE + (count % CHUNK_SIZE != 0);
	send(fd, (void *)&header, sizeof(struct s_header), MSG_WAITALL);
	ret = count;
	while (count)
	{
		if (count > CHUNK_SIZE)
			sub_header.size = CHUNK_SIZE;
		else
			sub_header.size = count;
		sub_header.nbr_chunk = 0;
		send(fd, (void *)&sub_header, sizeof(struct s_header), MSG_WAITALL);
		send(fd, data, sub_header.size, MSG_WAITALL);
		data = &(data[sub_header.size]);
		count -= sub_header.size;
	}
	return (ret);
}

void	send_end(int fd)
{
	t_header	header;

	header.size = 0;
	header.nbr_chunk = 0;
	send(fd, (void *)&header, sizeof(struct s_header), MSG_WAITALL);
}

ssize_t	recv_msg(int fd, char **data)
{
	t_header	header;
	int			ret;

	ret = recv(fd, (void *)&header, sizeof(struct s_header), MSG_WAITALL);
	if (ret <= 0 || header.size == 0)
		return (0);
	if (header.nbr_chunk == 0)
	{
		*data = ft_strnew(header.size);
		recv(fd, *data, header.size, MSG_WAITALL);
		return (header.size);
	}
	else
	{
		return (recv_msg(fd, data));
	}
}
