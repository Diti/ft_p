/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   iosock_files.c                                     :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: dtortera <dtortera@student.42.fr>          +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2014/05/17 10:50:50 by dtortera          #+#    #+#             */
/*   Updated: 2015/11/07 13:13:48 by dtortera         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "ftp.h"
#include "libft.h"
#include <errno.h>
#include <fcntl.h>
#include <stdio.h>
#include <sys/stat.h>

int		send_file(int sock, const char *filename)
{
	int			file_fd;
	char		buf[1024];
	struct stat	st;
	int			ret;

	if (-1 == (file_fd = open(filename, O_RDONLY)))
		return (error_fd(ft_strerror(errno), sock));
	errno = 0;
	if (-1 == fstat(file_fd, &st))
		return (error_fd(ft_strerror(errno), sock));
	while ((ret = read(file_fd, buf, CHUNK_SIZE)) > 0)
		send_data(sock, buf, ret);
	send_end(sock);
	return (0);
}

int		recv_file(int sock, const char *filename)
{
	int			file_fd;
	char		*data;
	struct stat	st;
	int			ret;

	if (-1 == (file_fd = open(filename, O_CREAT | O_WRONLY, 0666)))
		return (error_fd(ft_strerror(errno), sock));
	errno = 0;
	if (-1 == fstat(file_fd, &st))
		return (error_fd(ft_strerror(errno), sock));
	printf("BEGIN RECV FILE \033[33m%s\033[0m\n", filename);
	while ((ret = recv_msg(sock, &data)) > 0)
	{
		write(file_fd, data, ret);
		ft_strdel(&data);
	}
	printf("  END RECV FILE \033[33m%s\033[0m\n", filename);
	close(file_fd);
	return (0);
}
