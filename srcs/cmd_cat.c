/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   cmd_cat.c                                          :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: dtortera <marvin@42.fr>                    +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2015/10/13 14:14:00 by dtortera          #+#    #+#             */
/*   Updated: 2015/11/06 15:55:07 by dtortera         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "ftp.h"
#include "libft.h"
#include <errno.h>
#include <fcntl.h>

int	cmd_cat(t_server *serv, int c_sock, const char *cmd_line)
{
	int		fd;
	int		ret;
	char	*line;
	char	**files;

	line = NULL;
	(void)serv;
	files = ft_strsplit(cmd_line, ' ');
	if (-1 == (fd = open(files[1], O_RDONLY)))
		return (error_fd(ft_strerror(errno), c_sock));
	errno = 0;
	while (0 != (ret = get_next_line(fd, &line)))
	{
		if (ret == -1)
			return (error_fd(ft_strerror(errno), c_sock));
		send_msg(c_sock, line);
	}
	send_msg(c_sock, MSG_OK);
	return (NO_ERR);
}
