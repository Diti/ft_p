/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   cmd_pwd.c                                          :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: dtortera <marvin@42.fr>                    +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2015/10/13 14:17:58 by dtortera          #+#    #+#             */
/*   Updated: 2015/11/06 15:17:25 by dtortera         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include <stdio.h>
#include "ftp.h"
#include "libft.h"
#include <errno.h>
#include <stdio.h>
#include <unistd.h>

int	cmd_pwd(t_server *serv, int c_sock, const char *cmd_line)
{
	char	*out;
	char	*pwd;
	long	i;

	(void)cmd_line;
	pwd = getcwd(NULL, 0);
	if (!pwd)
		return (error_fd(ft_strerror(errno), c_sock));
	i = ft_strlen(pwd) - ft_strlen(serv->droot);
	if (i < 0)
		return (error_fd("Access forbidden above document root.", c_sock));
	else if (i == 0)
		out = "/";
	else
	{
		out = ft_strnew(i + 1);
		pwd += ft_strlen(serv->droot);
		ft_strcat(out, pwd);
	}
	send_msg(c_sock, out);
	send_msg(c_sock, MSG_OK);
	return (NO_ERR);
}
