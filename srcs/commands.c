/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   commands.c                                         :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: dtortera <dtortera@student.42.fr>          +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2014/05/17 17:55:41 by dtortera          #+#    #+#             */
/*   Updated: 2015/11/05 15:40:28 by dtortera         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "commands.h"
#include "ftp.h"
#include "libft.h"
#include <stdio.h>
#include <sys/wait.h>
#include <unistd.h>

int	do_nothing(t_server *serv, int c_sock, const char *cmd_line)
{
	(void)serv;
	(void)c_sock;
	(void)cmd_line;
	return (NO_ERR);
}

int	cmd_help(t_server *serv, int c_sock, const char *cmd_line)
{
	extern const struct s_cmd	g_commands[];
	char						cmds_txt[512];
	size_t						i;

	(void)serv;
	(void)cmd_line;
	i = -1;
	ft_strclr(cmds_txt);
	ft_strncat(cmds_txt, "Available commands:", 19);
	while (g_commands[++i].name != NULL)
	{
		ft_strncat(cmds_txt, " ", 1);
		ft_strcat(cmds_txt, g_commands[i].name);
	}
	ft_strncat(cmds_txt, " quit", 5);
	send_msg(c_sock, cmds_txt);
	send_msg(c_sock, MSG_OK);
	return (SUCCESS);
}
