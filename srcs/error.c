/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   error.c                                            :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: dtortera <dtortera@student.42.fr>          +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2014/05/16 18:33:31 by dtortera          #+#    #+#             */
/*   Updated: 2015/11/05 15:40:48 by dtortera         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include <stdlib.h>
#include "ftp.h"
#include "libft.h"

int		error_fd(const char *msg_desc, int fd)
{
	char	msg[512];

	ft_strclr(msg);
	ft_strcat(msg, MSG_FAIL);
	if (msg_desc && *msg_desc != '\0')
	{
		ft_strncat(msg, ": ", 2);
		ft_strcat(msg, msg_desc);
	}
	send_msg(fd, msg);
	return (FAILURE);
}

void	error_exit(const char *prefix, const char *err_msg, int err_num)
{
	if (prefix && *prefix != '\0')
	{
		ft_putstr_fd(prefix, 2);
		ft_putstr_fd(": ", 2);
	}
	ft_putendl_fd(err_msg, 2);
	exit(err_num);
}
